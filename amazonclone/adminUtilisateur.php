<?php
include "./includes/admin.php";
include "./dbconnect.php";

$action = (isset($_POST["act"])) ? $_POST["act"] : "";
$user_id = (isset($_POST["user_id"])) ? $_POST["user_id"] : "";

$cli_nom = "";
$cli_prenom = "";
$cli_email = "";
$cli_tel = "";
$cli_adresse = "";
$cli_admin = "";

if ($action == "N") {
    $cli_nom = (isset($_POST["cli_nom"])) ? $_POST["cli_nom"] : '';
    $cli_prenom = (isset($_POST["cli_prenom"])) ? $_POST["cli_prenom"] : '';
    $cli_email = (isset($_POST["cli_email"])) ? $_POST["cli_email"] : '';
    $cli_mdp = (isset($_POST["cli_mdp"])) ? $_POST["cli_mdp"] : '';
    $cli_tel = (isset($_POST["cli_tel"])) ? $_POST["cli_tel"] : '';
    $cli_adresse = (isset($_POST["cli_adresse"])) ? $_POST["cli_adresse"] : '';
    $cli_admin = (isset($_POST["cli_admin"])) ? $_POST["cli_admin"] : 0;

    if ($user_id != "") {
        $cmd = " update user_info set first_name='$cli_prenom', last_name='$cli_nom', email='$cli_email',
                                     mobile='$cli_tel' , address='$cli_adresse' , isAdmin='$cli_admin' 
                                   where user_id= '$user_id';
                ";
    }

    $dbc->query($cmd);

}
if ($action == "M") {

    $cmd = "select * from user_info where user_id = '$user_id' ;";
    $res = $dbc->query($cmd);
    $line = $res->fetch();

    $cli_id = $line["user_id"];
    $cli_nom = $line["last_name"];
    $cli_prenom = $line["first_name"];
    $cli_email = $line["email"];
    $cli_tel = $line["mobile"];
    $cli_adresse = $line["address"];
    $cli_admin = $line["isAdmin"];


}
if ($action == "S") {
    $cmd = "UPDATE user_info set deleted=1 where user_id='$user_id' ; ";
    $dbc->query($cmd);

}
if ($action == "Restaurer") {
    $cmd = "UPDATE user_info set deleted=0 where user_id='$user_id' ; ";
    $dbc->query($cmd);

}


$cmd = "select * from user_info";
$res = $dbc->query($cmd);
$table = $res->fetchAll();
?>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-2">
            <div class="nav nav-pills nav-stacked">
                <li class="active"><a href="#"><h4>Gestion</h4></a></li>
                <li><a href="#">Utilisateurs</a></li>
                <li><a href="adminStock.php">Stocks</a></li>
                <li><a href="adminStatistique.php">Statistiques</a></li>
            </div>
        </div>
        <div class="col-md-8">
            <h1>Gestion des utilisateurs</h1>
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">Prénom</th>
                    <th scope="col">Nom</th>
                    <th scope="col">Email</th>
                    <th scope="col">Téléphone</th>
                    <th scope="col">addresse</th>
                    <th scope="col">Administrateur</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tr>
                    <form action="" method="post" id="info">
                        <input type="hidden" name="act" value="N">
                        <input type="hidden" name="user_id" value="<?php echo $cli_id; ?>">
                        <td><input class="form-control" type="text" name="cli_prenom" value="<?php echo $cli_prenom; ?>"
                            ></td>
                        <td><input class="form-control" type="text" name="cli_nom" value="<?php echo $cli_nom; ?>">
                        </td>
                        <td><input class="form-control" type="text" name="cli_email" value="<?php echo $cli_email; ?>">
                        </td>
                        <td><input class="form-control" type="text" name="cli_tel" value="<?php echo $cli_tel; ?>">
                        </td>
                        <td><input class="form-control" type="text" name="cli_adresse"
                                   value="<?php echo $cli_adresse; ?>">
                        </td>
                        <td>
                            <select class="form-control" id="cli_admin" name="cli_admin">
                                <?php
                                if ($cli_admin == 0) { ?>
                                    <option value="0" selected>Non</option>
                                    <option value="1">Oui</option>
                                <?php } else { ?>
                                    <option value="0">Non</option>
                                    <option value="1" selected>Oui</option>
                                    <?php
                                }
                                ?>

                            </select>
                        </td>
                        <td><input class="btn btn-primary" type="submit" value="Valider"></td>
                        <td></td>
                    </form>
                </tr>

                <tbody>

                <?php foreach ($table as $row) {
                    if ($row["deleted"] == 0) { ?>
                        <tr>
                            <td><?php echo $row["first_name"] ?></td>
                            <td><?php echo $row["last_name"] ?></td>
                            <td><?php echo $row["email"] ?></td>
                            <td><?php echo $row["mobile"] ?></td>
                            <td><?php echo $row["address"] ?></td>
                            <td><?php echo $row["isAdmin"] == 0 ? "Non" : "Oui" ?></td>

                            <td>
                                <form action="" method="post">
                                    <input type="hidden" name="user_id" value="<?php echo $row["user_id"] ?>">
                                    <input type="hidden" name="act" value="M">
                                    <input type="submit" value="M" class="btn btn-info">
                                </form>
                            </td>
                            <td>
                                <form action="" method="post">
                                    <input type="hidden" name="act" value="S">
                                    <input type="hidden" name="user_id" value="<?php echo $row["user_id"] ?>">
                                    <input type="submit" value="S" class="btn btn-danger">
                                </form>
                            </td>
                        </tr>
                    <?php }
                } ?>

                </tbody>
            </table>
            <h1>Utilisateurs supprimés</h1>
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">Prénom</th>
                    <th scope="col">Nom</th>
                    <th scope="col">Email</th>
                    <th scope="col">Téléphone</th>
                    <th scope="col">addresse</th>
                    <th scope="col">Administrateur</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>

                <?php foreach ($table as $row) {
                    if ($row["deleted"] == 1) { ?>
                        <tr>
                            <td><?php echo $row["first_name"] ?></td>
                            <td><?php echo $row["last_name"] ?></td>
                            <td><?php echo $row["email"] ?></td>
                            <td><?php echo $row["mobile"] ?></td>
                            <td><?php echo $row["address"] ?></td>
                            <td><?php echo $row["isAdmin"] == 0 ? "Non" : "Oui" ?></td>

                            <td>
                                <form action="" method="post">
                                    <input type="hidden" name="act" value="Restaurer">
                                    <input type="hidden" name="user_id" value="<?php echo $row["user_id"] ?>">
                                    <input type="submit" value="Restaurer" class="btn btn-success">
                                </form>
                            </td>
                        </tr>
                    <?php }
                } ?>
                <tbody>

                </tbody>
            </table>
        </div>

    </div>
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>
<script src="assets/bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
<script src="main.js"></script>
</body>
<div class="foot">
    <footer>
    </footer>
</div>
<style> .foot {
        text-align: center;
    }
</style>
</html>
