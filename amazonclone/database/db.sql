-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 15, 2019 at 02:15 PM
-- Server version: 5.7.23
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `amaclone`
--

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `brand_id` int(100) NOT NULL,
  `brand_title` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`brand_id`, `brand_title`) VALUES
(1, 'HP'),
(2, 'Samsung'),
(3, 'Apple'),
(4, 'Sony'),
(5, 'LG'),
(6, 'Huawei'),
(7, 'Xiaomi'),
(8, 'OnePlus'),
(9, 'Tesla'),
(10, 'Boulanger'),
(11, 'Darty'),
(12, 'Philips'),
(13, 'BMW'),
(14, 'Audi');

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` int(10) NOT NULL,
  `p_id` int(10) NOT NULL,
  `ip_add` varchar(250) NOT NULL,
  `user_id` int(10) NOT NULL,
  `product_title` varchar(100) NOT NULL,
  `product_image` varchar(300) NOT NULL,
  `qty` int(100) NOT NULL,
  `price` int(100) NOT NULL,
  `total_amount` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`id`, `p_id`, `ip_add`, `user_id`, `product_title`, `product_image`, `qty`, `price`, `total_amount`) VALUES
(88, 1, '0.0.0.0', 2, 'Samsung S10', 'samsungs10.JPG', 1, 700, 700),
(89, 1, '0.0.0.0', 5, 'Samsung S10', 'samsungs10.JPG', 1, 700, 700),
(91, 2, '0.0.0.0', 6, 'iPhone X', 'iphonex.JPG', 1, 560, 560),
(92, 3, '0.0.0.0', 6, 'iPad Pro', 'ipadpro.jpg', 1, 1500, 1500);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `cat_id` int(11) NOT NULL,
  `cat_title` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`cat_id`, `cat_title`) VALUES
(1, 'Smartphones'),
(2, 'Accessories'),
(3, 'Laptops'),
(4, 'Desktops'),
(5, 'Screens'),
(6, 'Electric cars');

-- --------------------------------------------------------

--
-- Table structure for table `customer_order`
--

CREATE TABLE `customer_order` (
  `id` int(100) NOT NULL,
  `uid` int(100) NOT NULL,
  `pid` int(100) NOT NULL,
  `p_name` varchar(255) NOT NULL,
  `p_price` int(100) NOT NULL,
  `p_qty` int(100) NOT NULL,
  `p_status` varchar(100) NOT NULL,
  `tr_id` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer_order`
--

INSERT INTO `customer_order` (`id`, `uid`, `pid`, `p_name`, `p_price`, `p_qty`, `p_status`, `tr_id`) VALUES
(38, 1, 2, 'iPhone X', 560, 1, 'CONFIRMED', '1007035391'),
(39, 1, 2, 'iPhone X', 560, 1, 'CONFIRMED', '2015521990'),
(40, 1, 2, 'iPhone X', 560, 1, 'CONFIRMED', '1267742640'),
(42, 1, 1, 'Samsung S10', 700, 1, 'CONFIRMED', '667451852'),
(43, 1, 1, 'Samsung S10', 700, 1, 'CONFIRMED', '7175014'),
(44, 1, 1, 'Samsung S10', 700, 1, 'CONFIRMED', '776044818'),
(45, 1, 2, 'iPhone X', 560, 1, 'CONFIRMED', '776044818'),
(47, 1, 3, 'iPad Pro', 1500, 1, 'CONFIRMED', '1573963183'),
(48, 1, 4, 'Samsung Galaxy Tab A', 300, 1, 'CONFIRMED', '1573963183'),
(49, 2, 1, 'Samsung S10', 700, 1, 'CONFIRMED', '840133042'),
(50, 2, 2, 'iPhone X', 560, 1, 'CONFIRMED', '840133042'),
(51, 6, 1, 'Samsung S10', 700, 1, 'CONFIRMED', '2097650657'),
(52, 6, 1, 'Samsung S10', 700, 1, 'CONFIRMED', '1352067364'),
(53, 6, 2, 'iPhone X', 2240, 4, 'CONFIRMED', '557982721'),
(54, 6, 3, 'iPad Pro', 3000, 2, 'CONFIRMED', '557982721'),
(55, 6, 1, 'Samsung S10', 14000, 20, 'CONFIRMED', '655619374'),
(56, 6, 1, 'Samsung S10', 35000, 50, 'CONFIRMED', '1648971583'),
(57, 6, 1, 'Samsung S10', 3500, 5, 'CONFIRMED', '608631882'),
(58, 6, 1, 'Samsung S10', 700, 1, 'CONFIRMED', '188916392'),
(59, 6, 1, 'Samsung S10', 700, 1, 'CONFIRMED', '1507342524'),
(60, 6, 1, 'Samsung S10', 2100, 3, 'CONFIRMED', '826800660'),
(61, 6, 1, 'Samsung S10', 2800, 4, 'CONFIRMED', '339959816'),
(62, 6, 1, 'Samsung S10', 700, 1, 'CONFIRMED', '1281403964'),
(63, 6, 2, 'iPhone X', 2240, 4, 'CONFIRMED', '1281403964'),
(64, 6, 2, 'iPhone X', 560, 1, 'CONFIRMED', '1594279641');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `product_id` int(100) NOT NULL,
  `product_cat` varchar(100) NOT NULL,
  `product_brand` varchar(100) NOT NULL,
  `product_title` varchar(50) NOT NULL,
  `product_price` int(100) NOT NULL,
  `product_desc` text NOT NULL,
  `product_image` text NOT NULL,
  `product_keywords` text NOT NULL,
  `Stock` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `product_cat`, `product_brand`, `product_title`, `product_price`, `product_desc`, `product_image`, `product_keywords`, `Stock`) VALUES
(1, '1', '2', 'Samsung S10', 700, 'Samsung S10\r\n<br/>\r\nCapacité de stockage 128 GB\r\n<br/>\r\nTaille de l\'écran 6,1 \"', 'samsungs10.JPG', 'samsung mobile electronics', 67),
(2, '1', '3', 'iPhone X', 560, 'iPhone X ', 'iphonex.JPG', 'apple iphone mobile electronics', 43),
(3, '1', '3', 'iPad Pro', 1500, 'iPad Pro <br/> Ecran LED Retina 10,5\" - Resolution 2224 x 1668 pixels <br/>\r\n               Capacité 256 Go - Wifi 802.11 a/b/g/n/ac + Bluetooth 4.2 <br/>\r\n               Jusqu\'à 10 h d\'autonomie <br/>\r\n               Ultra fin : 6,1 mm - Poids : 469 g <br/>', 'ipadpro.jpg', 'apple ipad tablet', 45),
(4, '1', '2', 'Samsung Galaxy Tab A', 300, 'Samsung Galaxy Tab A <br/> Finition en aluminium <br/>\r\n                        Ecran panoramique et bordures fines de 6,5 mm <br/>\r\n                        Son compatible Dolby Atmos 3D', 'samsunggalaxytabA.JPG', 'samsung tablet electronics', 40),
(5, '3', '3', 'MacBook Pro', 2000, 'MacBook Pro 13 pouces - Gris sidéral\r\n<br/>\r\nAjouter aux favoris\r\n<br/>\r\nProcesseur Intel Core i5 quadricœur de 8e génération à 2,4 GHz (Turbo Boost jusqu’à 4,1 GHz)\r\n<br/>\r\nÉcran Retina avec affichage True Tone\r\n<br/>\r\nTouch Bar et Touch ID\r\n<br/>\r\nIntel Iris Plus Graphics 655\r\n<br/>\r\n8 Go de mémoire LPDDR3 à 2 133 MHz\r\n<br/>\r\nSSD de 256 Go\r\n<br/>\r\nQuatre ports Thunderbolt 3\r\n<br/>\r\nClavier rétroéclairé - Français', 'macbook.jpg', 'apple laptop macbook', 40);

-- --------------------------------------------------------
--
-- Table structure for table `user_info`
--

CREATE TABLE `user_info` (
  `user_id` int(10) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(300) NOT NULL,
  `password` varchar(100) NOT NULL,
  `mobile` varchar(10) NOT NULL,
  `address` varchar(300) NOT NULL,
  `isAdmin` tinyint(1) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_info`
--

INSERT INTO `user_info` (`user_id`, `first_name`, `last_name`, `email`, `password`, `mobile`, `address`, `isAdmin`, `deleted`) VALUES
(1, 'Fares', 'Kissoum', 'kissoumfares@gmail.com', 'f29fda580bc914dca191b2098300d2dd', '0782169458', '176 Rue des Rabats', 0, 0),
(5, 'Test', 'TEST', 'test@test.tes', '098f6bcd4621d373cade4e832627b4f6', '1234567890', 'test', 1, 0),
(6, 'Larry', 'ANDRIAMAMPIANINA', 'larry.andriamampianina@efrei.net', '63b9a38a5710d56a2b7e638ba403cc52', '0652474073', '10 Rue Dmitri Mendeleïev', 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`brand_id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `customer_order`
--
ALTER TABLE `customer_order`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`);



--
-- Indexes for table `user_info`
--
ALTER TABLE `user_info`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `brand_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;


--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `cat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `customer_order`
--
ALTER TABLE `customer_order`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `product_id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `user_info`
--
ALTER TABLE `user_info`
  MODIFY `user_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;


