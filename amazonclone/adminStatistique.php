<?php
include "./dbconnect.php";
include "./includes/admin.php";

$cmd = "select pid, p_name,SUM(p_price) as somme, SUM(p_qty) as nombre from customer_order group by p_name, pid order by pid;";
$res = $dbc->query($cmd);
$table = $res->fetchAll();
?>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-2">
            <div class="nav nav-pills nav-stacked">
                <li class="active"><a href="#"><h4>Gestion</h4></a></li>
                <li><a href="adminUtilisateur.php">Utilisateurs</a></li>
                <li><a href="adminStock.php">Stocks</a></li>
                <li><a href="#">Statistiques</a></li>
            </div>
        </div>
        <div class="col-md-8">
            <h1>Statistique des ventes</h1>
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">Id Produit</th>
                    <th scope="col">Produit</th>
                    <th scope="col">Prix Unitaire</th>
                    <th scope="col">Produit vendu</th>
                    <th scope="col">Bénéfice</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $total = 0;
                foreach ($table

                         as $row) {
                    $pid = $row["pid"];
                    $pname = $row["p_name"];
                    $money = $row["somme"];
                    $nb = $row["nombre"];
                    $unit_price = $money / $nb;
                    $total = $total + $money;
                    ?>
                    <tr>
                        <td><?php echo $pid ?></td>
                        <td><?php echo $pname ?></td>
                        <td><?php echo $unit_price ?></td>
                        <td><?php echo $nb ?></td>
                        <td><?php echo $money ?></td>
                    </tr>
                    <?php
                }
                ?>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>Total :</td>
                    <td><?php echo $total ?></td>

                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>
<script src="assets/bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
<script src="main.js"></script>
</body>
<div class="foot">
    <footer>
    </footer>
</div>
<style> .foot {
        text-align: center;
    }
</style>
</html>
