<?php

include('dbconnect.php');

$f_name = $_POST['f_name'];
$l_name = $_POST['l_name'];
$email = $_POST['email'];
$password = md5($_POST['password']);
$mobile = $_POST['mobile'];
$address = $_POST['address'];
$name = "/^[A-Z][a-zA-Z ]+$/";
$emailValidation = "/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9]+(\.[a-z]{2,4})$/";
$number = "/^[0-9]+$/";
$error = "";


if (empty($f_name) || empty($l_name) || empty($email) || empty($password) || empty($mobile) || empty($address)) {
    echo "<div class='alert alert-danger alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>Veuillez remplier tous les champs.</div>";
    exit(0);
} else {
    $error = false;
    if (!preg_match($name, $f_name)) {
        echo "
			<div class='alert alert-warning'>
				<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
				<b>Le prénom $f_name n'est pas valide..!</b>
			</div>
		";
        $error = true;
    }

    if (!preg_match($name, $l_name)) {
        echo "
			<div class='alert alert-warning'>
				<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
				<b>Le nom $l_name n'est pas valide..!</b>
			</div>
		";
        $error = true;
    }

    if (!preg_match($emailValidation, $email)) {
        echo "
			<div class='alert alert-warning'>
				<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
				<b>L'email $email n'est pas valide..!</b>
			</div>
		";
        $error = true;
    }

    if (strlen($password) < 9) {
        echo "
			<div class='alert alert-warning'>
				<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
				<b>Mot de passe faible</b>
			</div>
		";
        $error = true;
    }

    if (!preg_match($number, $mobile)) {
        echo "
			<div class='alert alert-warning'>
				<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
				<b>Le numéro de téléphone $mobile n'est pas valide</b>
			</div>
		";
        $error = true;
    }

    if (!(strlen($mobile) == 10)) {
        echo "
			<div class='alert alert-warning'>
				<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
				<b>Le numéro de téléphone doit contenir 10 chiffres</b>
			</div>
		";
        $error = true;
    }

    if ($error == true) {
        exit();
    }

    //check for available user-details
    $sql = "SELECT user_id FROM user_info WHERE email = '$email' LIMIT 1";
    $res = $dbc->query($sql);
    $count_email = $res->rowCount();

    if ($count_email > 0) {
        echo "
			<div class='alert alert-danger'>
				<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
				<b>Email déjà utilisé, veuillez utiliser un autre email.</b>
			</div>
		";
        exit();
    } else {
        $sql = "INSERT INTO user_info (first_name, last_name, email, password, mobile, address, isAdmin) VALUES ('$f_name','$l_name','$email','$password','$mobile','$address', false )";
        $dbc->query($sql);
        echo "
								<div class='alert alert-success'>
									<a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
									Cliquez <b><a href='index.php'>ici</a></b> pour se connecter.
								</div>
						";

    }
}


?>