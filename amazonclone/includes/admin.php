<?php
session_start();
if (!isset($_SESSION['isAdmin'])) {
    header('Location:index.php');
}

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>TechStop</title>
    <link rel="stylesheet" type="text/css" href="http://kenwheeler.github.io/slick/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="http://kenwheeler.github.io/slick/slick/slick-theme.css"/>
    <link rel="stylesheet" type="text/css" href="assets/bootstrap-3.3.6-dist/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="styles.css">
    <link rel="shortcut icon" type="image/png" href="assets/prod_images/logo.png">
</head>
<body>
<div class="container">
    <div class="navbar navbar-default navbar-fixed-top" id="topnav">
        <div class="container-fluid">
            <div class="navbar-header">
                <h1 style="margin-left: 400px">Bienvenue sur le compte administrateur</h1>
            </div>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#" class="dropdown-toggle" data-toggle="dropdown"><span
                                class="glyphicon glyphicon-user"></span>Bonjour, <?php echo $_SESSION['uname']; ?></a>
                    <ul class="dropdown-menu">
                        <li><a href="logout.php">Déconnexion</a></li>
                    </ul>
                </li>
            </ul>

        </div>
    </div>
</div>

<br><br><br><br>