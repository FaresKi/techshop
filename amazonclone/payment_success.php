<?php
session_start();
include('dbconnect.php');
if (!isset($_SESSION['uid'])) {
    header('Location:index.php');
}
$uid = $_SESSION['uid'];

$trid = $_SESSION['transactionID'];
$sql = "SELECT * FROM customer_order WHERE uid='$uid' AND tr_id = '$trid'";
$run_query = $dbc->query($sql);
$row = $run_query->fetch();
$price = $row['p_price'];
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>TechShop</title>
    <link rel="shortcut icon" type="image/png" href="assets/prod_images/logo.png">
    <link rel="stylesheet" type="text/css" href="assets/bootstrap-3.3.6-dist/css/bootstrap.css">
    <style type="text/css">
        .content {
            display: none;
        }
    </style>
</head>
<body>
<div class='content'>
    <div class="navbar navbar-default navbar-fixed-top" id="topnav">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="index.php" class="navbar-header"><img
                            src="assets/prod_images/logo.png" alt="TechShop"
                            height="65px"></a></div>
        </div>
    </div>
</div>
<br><br><br><br><br>
<div class='container-fluid'>
    <div class='row'>
        <div class='col-md-2'></div>
        <div class='col-md-8'>
            <div class="panel panel-default">
                <div class="panel-heading"><h1>Merci !</h1></div>
                <div class="panel-body">
                    Bonjour <?php echo $_SESSION['uname']; ?>,
                    <br>Veuillez choisir un moyen de paiement.
                </div>
            </div>
            <div class='col-md-2'></div>
        </div>

    </div>
    <div id="paypal-button-container" align="center"></div>

    <!-- Include the PayPal JavaScript SDK -->
    <script src="https://www.paypal.com/sdk/js?client-id=sb&currency=EUR"></script>

    <script>
        // Render the PayPal button into #paypal-button-container
        paypal.Buttons({

            // Set up the transaction
            createOrder: function(data, actions) {
                return actions.order.create({
                    purchase_units: [{
                        amount: {
                            value: JSON.parse(<?php echo $price ?>)
                        }
                    }]
                });
            },

            // Finalize the transaction
            onApprove: function(data, actions) {
                return actions.order.capture().then(function(details) {
                    // Show a success message to the buyer
                    alert('Transaction completed by ' + details.payer.name.given_name + '!');
                });
            }


        }).render('#paypal-button-container');
    </script>

</div>
</div>
<!--Pre-loader -->
<div align="center" class="preload"><img src="assets/prod_images/transactionsuccess.gif" style="width:400px;
    height: 400px
    position: center;
    top: 0px;
    left: 469px;"></div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="assets/bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>

<script type="text/javascript">


    $(".preload").fadeOut(5000, function () {
        $(".content").fadeIn(500);
    });

</script>
</body>
</html>