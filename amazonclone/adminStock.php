<?php
include "./dbconnect.php";
include "./includes/admin.php";

$action = (isset($_POST["act"])) ? $_POST["act"] : "";
$product_id = (isset($_POST["product_id"])) ? $_POST["product_id"] : "";
$quantity = (isset($_POST["quantity"])) ? $_POST["quantity"] : 1;
$error = false;

if ($action == "Commander") {
    if ($quantity < 0) {
        $quantity = 0;
        $error = true;
    }
    $stock = (isset($_POST["stock"])) ? $_POST["stock"] : 0;
    $sum = $stock + $quantity;
    $cmd = "UPDATE products set Stock=$sum where product_id='$product_id';";
    $dbc->query($cmd);
}

$cmd = "select * from products";
$res = $dbc->query($cmd);
$table = $res->fetchAll();

?>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-2">
            <div class="nav nav-pills nav-stacked">
                <li class="active"><a href="#"><h4>Gestion</h4></a></li>
                <li><a href="adminUtilisateur.php">Utilisateurs</a></li>
                <li><a href="#">Stocks</a></li>
                <li><a href="adminStatistique.php">Statistiques</a></li>
            </div>
        </div>
        <div class="col-md-8">
            <?php
            if ($error == true) { ?>
                <div class='alert alert-warning'>
                    <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                    <b>Nombre doit être positif.</b>
                </div>
                <?php
            }
            ?>


            <h1>Gestion des stocks</h1>
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">Id produit</th>
                    <th scope="col">Produit</th>
                    <th scope="col">Prix</th>
                    <th scope="col">Quantité disponible</th>
                    <th scope="col">Quantité à commander</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($table as $row) { ?>
                    <form action="" method="post">
                        <tr>
                            <td><?php echo $row["product_id"] ?></td>
                            <td><?php echo $row["product_title"] ?></td>
                            <td><?php echo $row["product_price"] ?></td>
                            <td><?php echo $row["Stock"] ?></td>
                            <td>
                                <input class="form-control" type="number" name="quantity"
                                       value="<?php echo $quantity ?>">
                            </td>
                            <td>
                                <input type="hidden" name="product_id" value="<?php echo $row["product_id"] ?>">
                                <input type="hidden" name="stock" value="<?php echo $row["Stock"] ?>">
                                <input type="hidden" name="act" value="Commander">
                                <input type="submit" value="Valider" class="btn btn-info">
                            </td>
                        </tr>
                    </form>
                    <?php
                } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>
<script src="assets/bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
<script src="main.js"></script>
</body>
<div class="foot">
    <footer>
    </footer>
</div>
<style> .foot {
        text-align: center;
    }
</style>
</html>
